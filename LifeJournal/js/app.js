// References to DOM Elements
const previousButton = document.getElementById('previous-button');
const nextButton = document.getElementById('next-button');
const journal = document.getElementById('journal');

const page1 = document.getElementById('page1');
const page2 = document.getElementById('page2');
const page3 = document.getElementById('page3');

// Event Listener
previousButton.addEventListener('click', moveToPreviousPage)
nextButton.addEventListener('click', moveToNextPage)

// Business Logic
let currentLocation = 1
let numberOfPages = 3
let maximumLocation = numberOfPages + 1;

function openJournal(){
  journal.style.transform = "translateX(50%)";
  previousButton.style.transform = "translateX(-180px)";
  nextButton.style.transform = "translateX(180px)";
}

function closeJournal(isAtBeginning){
  if(isAtBeginning){
    journal.style.transform = "translateX(0%)";
  } else {
    journal.style.transform = "translateX(100%)";
  }

  previousButton.style.transform = "translateX(0%)";
  nextButton.style.transform = "translateX(0%)";
}

function moveToNextPage(){
  if(currentLocation < maximumLocation){
    switch (currentLocation) {
      case 1:
        openJournal();
        page1.classList.add("flipped")
        page1.style.zIndex = 1;
        break;
      case 2:
        page2.classList.add("flipped")
        page2.style.zIndex = 2;
        break;
      case 3:
        page3.classList.add("flipped")
        page3.style.zIndex = 3;
        closeJournal(false)
        break;
      default:
        throw new Error("Unknown location");
    }
    currentLocation++;
  }
}

function moveToPreviousPage(){
  if(currentLocation > 1){
    switch (currentLocation) {
      case 2:
        closeJournal(true);
        page1.classList.remove("flipped")
        page1.style.zIndex = 3;
        break;
      case 3:
        page2.classList.remove("flipped")
        page2.style.zIndex = 2;
        break;
      case 4:
        openJournal()
        page3.classList.remove("flipped")
        page3.style.zIndex = 1;
        break;
      default:
        throw new Error("Unknown location");
    }
    currentLocation--;
  }
}
